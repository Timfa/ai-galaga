﻿using UnityEngine;
using System.Collections;

public class GalagaMovement : MonoBehaviour {

	public float speed = 1f;

	void Update () {
		// get origin position
		var position = gameObject.transform.position;

        float lspeed = Mathf.Clamp01((float)GetComponent<GalagaBrain>().brainOutput[0]);
        float rspeed = Mathf.Clamp01((float)GetComponent<GalagaBrain>().brainOutput[1]);

        float cspeed = -lspeed + rspeed;
        
        // add delta, and set range
        gameObject.transform.position = new Vector2 (
			Mathf.Clamp(position.x + cspeed * speed, -6f, 6f)
			, position.y
		);
	}
}
