﻿using System.Collections;
using System.Collections.Generic;
using TimGameV2.Engine;
using UnityEngine;

public class BrainSaver : MonoBehaviour {

    private float timer = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnApplicationQuit() {
        
        GalagaBrain.BrainBox box = new GalagaBrain.BrainBox();

        box.agent = GalagaBrain.brain.ToString();

        box.SaveAsFile("galaga.brain");
	}
}
