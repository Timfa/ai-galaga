﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{

    private float t = -1;

    public void Move()
    {
        if (t < 0)
            t = Random.value * 10;

        Vector2 pos = transform.position;
        t += Time.deltaTime;

        if (transform.position.y < -5.5f)
        {
            transform.position += Vector3.up * 11f;
        }

        var speed = 1.0f;
        transform.position -= Vector3.up * Time.deltaTime * speed;
        pos.x = Mathf.Sin(t) * 6.5f;

        if (transform.position.x < pos.x)
            transform.position += transform.right * 4 * Time.deltaTime;

        if (transform.position.x > pos.x)
            transform.position -= transform.right * 4 * Time.deltaTime;
    }

}
