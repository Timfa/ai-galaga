﻿using System.Collections;
using System.Collections.Generic;
using TimGameV2.Engine;
using UnityEngine;

public class GalagaBrain : MonoBehaviour
{
    public static NeuralNetwork.ReinforcedAgent brain;

    public static List<GameObject> myBullets = new List<GameObject>();
    [SerializeField] private int rays;
    [SerializeField] private float raySpacing;

    public BrainBox box = new BrainBox();

    public static float noKillTimer = 5;

    [SerializeField] private TextMesh learns, resets, shoot, move, moveR;

    public double[] brainOutput = new double[2];

	// Use this for initialization
	void Start ()
    {
	    if(brain == null)
        {
            BrainBox box = DataSaver.LoadFile<BrainBox>("galaga.brain");
            if (box != null)
            {
                brain = NeuralNetwork.ReinforcedAgent.Parse(box.agent);
                Debug.Log("loaded brain");
            }
            
            if(brain == null)
            {
                NeuralNetwork.Network network = new NeuralNetwork.Network(rays + 2);
                network.AddLayer(rays + 2, NeuralNetwork.Neuron.ActivationTypes.Linear);
                network.AddLayer(3, NeuralNetwork.Neuron.ActivationTypes.Sigmoid);

                brain = new NeuralNetwork.ReinforcedAgent(network, 10, 10);
                Debug.Log("Did not load brain");
            }
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        myBullets.RemoveAll(o => o == null);

        noKillTimer -= Time.deltaTime;

        if(noKillTimer < 0)
        {
            noKillTimer = 1;
            brain.Punish(1);
        }

        learns.text = brain.Evolutions.ToString();
        resets.text = brain.Resets.ToString();

        double[] inputs = new double[rays + 2];
        
        GetComponent<EdgeCollider2D>().enabled = false;
        foreach (GameObject b in myBullets)
            b.GetComponent<BoxCollider2D>().enabled = false;

        for(int i = 0; i < rays - 2; i++)
        {
            float xP = raySpacing + ((-rays / 2) * raySpacing + i * raySpacing);

            RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(xP, -.5f, 0), transform.up, 10);

            if (hit.collider != null)
            {
                bool isBlt = hit.transform.GetComponent<Bullet>() != null;
                inputs[i] = 10 - hit.distance;

                if (isBlt)
                   inputs[i] *= -1;

                Debug.DrawLine(transform.position + new Vector3(xP, -.5f, 0), hit.point, isBlt? Color.red : Color.blue);
            }
            else
            {
                inputs[i] = 0;
                Debug.DrawLine(transform.position + new Vector3(xP, -.5f, 0), transform.position + new Vector3(xP, 9.5f, 0), Color.white);
            }
        }

        inputs[inputs.Length - 1] = Mathf.Clamp(6.6f - transform.position.x, 0, 1000);
        inputs[inputs.Length - 2] = Mathf.Clamp((-6.6f +transform.position.x) * -1, 0, 1000);

        GetComponent<EdgeCollider2D>().enabled = true;
        foreach (GameObject b in myBullets)
            b.GetComponent<BoxCollider2D>().enabled = true;

        brainOutput = brain.GetOutput(inputs);

        move.text = (Mathf.Round((float)brainOutput[0] * 100) / 100).ToString();
        moveR.text = (Mathf.Round((float)brainOutput[1] * 100) / 100).ToString();
        shoot.text = (brainOutput[2] > 0).ToString();
	}

    [System.Serializable]
    public class BrainBox
    {
        public string agent;
    }
}
